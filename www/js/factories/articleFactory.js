angular.module('hdm').service('ArticleFactory', ['DownloadFilesSrv', '$http', '$q', function (DownloadFilesSrv, $http, $q) {

	var that = this;

	this.buildArticlesFromResponse = function (data){
		var items = data.nodes;
		var promises = [];

		for(i in items){
			var item = items[i].node;
			var promise = this.buildArticle(item);
			promises.push(promise);
		}

		return $q.all(promises);
	};

	this.buildArticle = function(item){

		//Apaño para la URL
		var anchor = item.public_url;
		var public_url = anchor.match(/href="([^"]*)/)[1];

		//Apaño para los enlaces dentro del body
		var body = item.body;
		body = body.replace(/href/g, 'ng-click="openLink($event)" href');

		var article = {
			"nid": item.nid,
			"title": item.title,
			"image_url": item.imagen.src,
			"body_resume": item.body_resume,
			"body": body,
			"url": item.url,
			"created": item.created,
			"created_sortable": item.created_sortable,
			"changed": item.last_update,
			"author_name": item.author_name,
			"public_url": public_url,
			// categorias
			// comentarios
		};

		//Descargamos la imagen
		promise = DownloadFilesSrv.getLocalUrlOfFile(article);

		return promise;
	};
}]);