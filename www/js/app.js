angular.module('hdm', ['ionic']).run(function($ionicPlatform){

	var registerForSNS = function(gcmId){
		var sns = new AWS.SNS();
		
		AWS.config.region = 'eu-west-1';
		AWS.config.update({
			credentials : {
				"accessKeyId": "AKIAIQ344FZ2CYZRVCDQ",
				"secretAccessKey":"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"
			}
		});

		var params = {
			//PlatformApplicationArn: 'arn:aws:sns:eu-west-1:972295457311:app/APNS_SANDBOX/HDManzanas', /* required */
			PlatformApplicationArn: "arn:aws:sns:eu-west-1:972295457311:hdm-test",
			Token: gcmId, /* required */
			CustomUserData: 'STRING_VALUE'
		};

		sns.createPlatformEndpoint(params, function(err, data) {
			if (err){
				//Error
				console.log(err, err.stack);
			}else{
				console.log(data.EndpointArn);

				var params = {
					Protocol: 'STRING_VALUE', /* required */
					TopicArn: 'STRING_VALUE', /* required */
					Endpoint: 'STRING_VALUE'
				};
				sns.subscribe(params, function(err, data) {
				if (err) console.log(err, err.stack); // an error occurred
				else     console.log(data);           // successful response
				});
			}
		});
	};

	var initPushNotifications = function(){
		var push = PushNotification.init({
			android: {
				senderID: "XXXXXXX"
			},
			browser: {
				pushServiceURL: 'http://push.api.phonegap.com/v1/push'
			},
			ios: {
				alert: "true",
				badge: "true",
				sound: "true"
			},
			windows: {}
		});

		push.on('registration', function(data) {
			console.log("registration");
			console.log(data);
			if (data.registrationId.length > 0 ) {
				registerForSNS(data.registrationId); 
			}
		});

		push.on('notification', function(data) {
			console.log("notification");
			console.log(data);
		});

		push.on('error', function(e) {
		   console.log("error");
		   console.log(e);
		});
	};

	$ionicPlatform.ready(function() {
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			cordova.plugins.Keyboard.disableScroll(true);
		}
		if (window.StatusBar) {
			StatusBar.styleLightContent();
			StatusBar.backgroundColorByHexString('#428bca'); 
		}
		if (window.PushNotification){
			initPushNotifications();
		}
	});
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $sceDelegateProvider){
	$stateProvider

	.state('app', {
		url: '/app',
		abstract: true,
		templateUrl: 'templates/menu.html',
		controller: 'AppCtrl'
	})

	.state('app.home', {
		url: '/home',
		views: {
			'menuContent': {
				templateUrl: 'templates/home.html',
				controller: 'HomeCtrl'
			}
		}
	})

	.state('app.articulo', {
		url: '/home/articulo',
		views: {
			'menuContent': {
				templateUrl: 'templates/articulo.html',
				controller: 'ArticuloCtrl'
			}
		},
		params: {
			articulo: null
		}
	})

	.state('app.categorias', {
		url: '/categorias',
		views: {
			'menuContent': {
				templateUrl: 'templates/categorias.html',
				controller: 'CatCtrl'
			}
		}
	})

	.state('app.videos', {
		url: '/videos',
		views: {
			'menuContent': {
				templateUrl: 'templates/videos.html',
				controller: 'VideosCtrl'
			}
		}
	});

	//El interpolate se queja si no generas urls al vuelo que no estén permitidas
	$sceDelegateProvider.resourceUrlWhitelist(['self', 'https://www.youtube.com/**']);

	// Centered Navbar
	// Doesnt work well if one icon at the navbar
	//$ionicConfigProvider.navBar.alignTitle('center');

	// Native scrolling for Android
	var jsScrolling = (ionic.Platform.isAndroid() ) ? false : true;
	$ionicConfigProvider.scrolling.jsScrolling(jsScrolling);

	//No text on ios back button
	$ionicConfigProvider.backButton.previousTitleText(false).text('');

	// if none of the above states are matched, use this as the fallback
	$urlRouterProvider.otherwise('/app/home');
});