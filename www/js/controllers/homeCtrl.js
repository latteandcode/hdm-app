angular.module('hdm').controller('HomeCtrl', ['$rootScope', '$ionicScrollDelegate', '$scope', '$timeout', 'FeedSrv', '$ionicLoading', 'ArticleStoreSrv', '$ionicPopup', '$state', '$ionicPlatform',
	function ($rootScope, $ionicScrollDelegate, $scope, $timeout, FeedSrv, $ionicLoading, ArticleStoreSrv, $ionicPopup, $state, $ionicPlatform) {

	// Expongo el state para mostrar el botón de compartir si estás en un articulo
	$rootScope.state = $state;
	$rootScope.page = 0;
	$rootScope.newsFromLocal = false;
	$scope.moreNews = false;
	$scope.lastestNews = [];
	$scope.loadingNews = true;

	$scope.showLoading = function(){
		$ionicLoading.show({
			templateUrl: 'templates/loading-articles.html'
		});
	};

	$scope.hideLoading = function(){
		$ionicLoading.hide();
	};

	$scope.getLastestNews = function(showIonicLoading){
		console.log("getLastestNews");
		$scope.loadingNews = true;
    	//$scope.lastestNews = [];
		$scope.moreNews = false;
		$rootScope.page = 0;

		if(showIonicLoading){
			$scope.showLoading();
		}

		var promise = FeedSrv.getLastestNews($rootScope.page);
		promise.then(function (response) {
			$rootScope.newsFromLocal = false;
			$scope.lastestNews = response;
			ArticleStoreSrv.saveArticles($scope.lastestNews);
			$scope.hideLoading();
			$scope.$broadcast('scroll.refreshComplete');

			//Le damos un timeout para que se pinten las noticias, si no se llamaría a getMoreNews sin querer
			$timeout($scope.thereAreMoreNews, 3000);
			$timeout($scope.finishLoadingNews, 3000);
		}, function (error) {
			$rootScope.newsFromLocal = true;
			console.log(error);
			$scope.hideLoading();
			$scope.$broadcast('scroll.refreshComplete');

			var alertPopup = $ionicPopup.alert({
				title: 'Error de conexión :(',
				template: 'No se han podido descargar las últimas noticias. Mientras tanto, puedes leer las que ya están descargadas.'
			});

			$scope.lastestNews = ArticleStoreSrv.getArticles($rootScope.page);
			//Le damos un timeout para que se pinten las noticias, si no se llamaría a getMoreNews sin querer
			$timeout($scope.thereAreMoreNews, 3000);
			$timeout($scope.finishLoadingNews, 3000);
		});
	};

	$scope.getMoreNews = function(){
		$timeout($scope.getMoreNewsAction, 2000);
	};

	$scope.getMoreNewsAction = function(){
		var scrollPosition = $ionicScrollDelegate.getScrollPosition().top;

		if(scrollPosition > 500){
			if($scope.lastestNews.length && $scope.moreNews && !$scope.loadingNews){
				console.log("getMoreNews");
				$scope.loadingNews = true;
				$rootScope.page = $rootScope.page + 1;
				if($scope.newsFromLocal){
					$scope.getMoreNewsFromLocal();
				}else{
					$scope.getMoreNewsFromWeb();
				}
			}else{
				console.log("no getMoreNews");
				$scope.$broadcast('scroll.infiniteScrollComplete');
			}
		}else{
			$scope.$broadcast('scroll.infiniteScrollComplete');
			console.log("Detectado error del infinite scroll");
		}
	};

	$scope.getMoreNewsFromWeb = function(){
		console.log("getMoreNewsFromWeb");
		var promise = FeedSrv.getLastestNews($rootScope.page);
		promise.then(function (response) {
			if(response.length == 0){
				$scope.moreNews = false;
			}else{
				ArticleStoreSrv.saveArticles(response);
				$scope.lastestNews = $scope.lastestNews.concat(response);
			}
			$scope.$broadcast('scroll.infiniteScrollComplete');
			$timeout($scope.finishLoadingNews, 3000);
		}, function (error) {
			console.log(error);
			$scope.$broadcast('scroll.infiniteScrollComplete');
			$timeout($scope.finishLoadingNews, 3000);
		});
	};

	$scope.getMoreNewsFromLocal = function(){
		console.log("getMoreNewsFromLocal");
		
		var moreNews = ArticleStoreSrv.getArticles($rootScope.page);
		if(moreNews.length == 0){
			$scope.moreNews = false;
		}else{
			$scope.lastestNews = $scope.lastestNews.concat(moreNews);
		}

		$scope.$broadcast('scroll.infiniteScrollComplete');
		$timeout($scope.finishLoadingNews, 3000);
	};

	$scope.viewArticle = function($event, article){
		// Ponemos clase selected y la quitamos en 500ms
		var element = angular.element($event.currentTarget);
		element.addClass('selected');
		$timeout(function(){ 
			element.removeClass('selected');
		}, 500);

		$state.go("app.articulo", {
			"articulo": article
		});
	};

	$scope.finishLoadingNews = function(){
		$scope.loadingNews = false;
	};

	$scope.thereAreMoreNews = function(){
		$scope.moreNews = true;
	};

    // Mostramos cargando
    $scope.showLoading();

    // LLamamos a getLastestNews cuando device ready
    // asi prevenimos el fallo de que FileTransfer no esté disponible
	$ionicPlatform.ready(function() {
		$scope.getLastestNews(false);
	});
}]);