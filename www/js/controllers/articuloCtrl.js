angular.module('hdm').controller('ArticuloCtrl', ['$scope', '$rootScope', '$stateParams', '$timeout', '$ionicPopup', '$ionicLoading', function ($scope, $rootScope, $stateParams, $timeout, $ionicPopup, $ionicLoading) {

	$scope.new = $stateParams.articulo;
	$scope.textCopied = false;

	// Opciones para el sharing
	var options = {
		message: $scope.new.title,
		subject: $scope.new.title, // fi. for email
		files: null, // an array of filenames either locally or remotely
		url: 'https://hablandodemanzanas.com/guia-de-compra-noticias/las-mejores-promociones-y-ofertas-del-dia-de-amazon-black-friday-navidad-descuentos',
		chooserTitle: 'Elige cómo compartir el artículo' // Android only, you can override the default share sheet title
	};

	$scope.showLoading = function(){
		$ionicLoading.show({
			templateUrl: 'templates/loading-external-url.html'
		});
	};

	$scope.hideLoading = function(){
		$ionicLoading.hide();
	};

	var onSuccess = function(result) {
		$scope.closePopup();
	};

	var onError = function(msg){
		$scope.closePopup();
		$scope.showAlert("Lo sentimos, ha ocurrido un error durante la publicación.");
	};

	$scope.shareThisArticleGeneric = function(){
		window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);
	}

	$scope.shareThisArticle = function(overrideOpenLinkUrl){
		if(overrideOpenLinkUrl){
			$scope.openLinkUrl = $scope.new.public_url;
		}

		$scope.closePopup();
		$scope.myPopup = $ionicPopup.show({
			templateUrl: 'templates/share-modal.html',
			scope: $scope,
		});
	};

	//Android: OK
	//iOS: Por revisar
	$scope.shareThisArticleViaFacebook = function(){
		window.plugins.socialsharing.shareViaFacebook(
			null, // text
			null, // IMG 
			$scope.openLinkUrl, // URL 
			function(){
				$scope.closePopup();
			},
			function(errormsg){
				if(errormsg != "cancelled"){
					$scope.showAlert("Parece que no tienes instalado Facebook. Descarga la app para poder compartir el artículo.");
				}
			}
		);
	};

	//Android: OK
	//iOS: Por revisar
	$scope.shareThisArticleViaTwitter = function(){
		var message = $scope.new.title;

		if(message.length > 99){
			message = message.substring(0,96);
			message = message + "...";
		}
		message = message + "\n" + $scope.openLinkUrl + "\n";
		message = message + "\nVia @hdmanzanas";

		window.plugins.socialsharing.shareViaTwitter(
			message, // text
			$scope.new.image_local_url, //img
			null, //url
			function(){
				$scope.closePopup();
			},
			function(errormsg){
				if(errormsg != "cancelled"){
					$scope.showAlert("Parece que no tienes instalado Twitter. Descarga la app para poder compartir el artículo.");
				}
			}
		);
	};

	//Android: Revisar
	//iOS: Por revisar
	$scope.shareThisArticleViaWhatsApp = function(){
		window.plugins.socialsharing.shareViaWhatsApp(
			null, //message
			null, //img
			$scope.openLinkUrl, //url
			function(){
				$scope.closePopup();
			},
			function(errormsg){
				if(errormsg != "cancelled"){
					$scope.showAlert("Parece que no tienes instalado WhastApp. Descarga la app para poder compartir el artículo.");
				}
			}
		);
	};

	//Android: OK
	//iOS: Por revisar
	$scope.shareThisArticleViaInstagram = function(){
		window.plugins.socialsharing.shareViaInstagram(
			$scope.new.title + " via @hdmanzanas", //text
			$scope.new.image_local_url, //img
			function(){
				$scope.closePopup();
			},
			function(errormsg){
				if(errormsg != "cancelled"){
					$scope.showAlert("Parece que no tienes instalado Instagram. Descarga la app para poder compartir el artículo.");
				}
			}
		);
	};

	$scope.closePopup = function(){
		if($scope.myPopup){
			$scope.myPopup.close();
		}
	}

	$scope.showAlert = function(message){
		var alertPopup = $ionicPopup.alert({
			title: "Error",
			template: message
		});
	};

	$scope.openLink = function(event){
		event.preventDefault();
		$scope.openLinkUrl = event.currentTarget.href;

		$scope.myPopup = $ionicPopup.show({
			templateUrl: 'templates/click-on-link-modal.html',
			scope: $scope,
		});
	};

	$scope.openInAppBrowserInside = function(location){
		$scope.closePopup();
		$scope.showLoading();

		//opciones android
		var options = 'location=yes,enableviewportscale=no,zoom=no';

		//ios
		if(ionic.Platform.isIOS()){
			options = 'location=no,enableviewportscale=no,zoom=no,closebuttoncaption=Volver a la APP,toolbarposition=top';
		}
		
		//Ponemos un timeout porque no se ve el cargando...
		$timeout(function(){
			var ref = cordova.InAppBrowser.open($scope.openLinkUrl, "_blank", options);
			ref.addEventListener("exit", $scope.hideLoading);
		}, 1500);
	};

	$scope.openInAppBrowserSystem = function(location){
		$scope.closePopup();
		$scope.showLoading();
		
		// Ponemos un timeout porque no se ve el cargando...
		$timeout(function(){
			var ref = cordova.InAppBrowser.open($scope.openLinkUrl, "_system");
		}, 1500);

		// Pasados 2seg de la apertura quitamos el loading
		$timeout(function(){
			$scope.hideLoading();
		}, 3500);
	};

	$scope.copyLinkToClipboard = function(link) {
        cordova.plugins.clipboard.copy(link);

        // Si no hay timeout el activated y el verde se ven feos
        $timeout(function(){
			$scope.textCopied = true;
		}, 300);
        
        //Cerramos modal y el copiado!
        $timeout(function(){
			$scope.closePopup();
		}, 1500);

		$timeout(function(){
			$scope.textCopied = false;
		}, 1750);
    };

	//Expongo la función
	$rootScope.shareThisArticle = $scope.shareThisArticle;
}]);