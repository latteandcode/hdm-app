angular.module('hdm').controller('VideosCtrl', ['$rootScope', '$scope', 'VideosFeedSrv', '$ionicLoading', '$ionicPopup',
	function ($rootScope, $scope, VideosFeedSrv, $ionicLoading, $ionicPopup) {

	$scope.lastestVideos = [];

	$scope.showLoading = function(){
		$ionicLoading.show({
			templateUrl: 'templates/loading-videos.html'
		});
	};

	$scope.hideLoading = function(){
		$ionicLoading.hide();
	};

	$scope.getLastestVideos = function(){
		console.log("getLastestVideos");

		// Mostramos cargando
    	$scope.showLoading();

		var promise = VideosFeedSrv.getLastestVideos();
		promise.then(function (response){
			$scope.lastestVideos = response;
			console.log($scope.lastestVideos);
			$scope.hideLoading();
		}, function (error){
			$scope.hideLoading();

			var alertPopup = $ionicPopup.alert({
				title: 'Error de conexión :(',
				template: 'No se han podido descargar los vídeos.'
			});
		});
	};

	$scope.getIframeSrc = function (videoId) {
		return 'https://www.youtube.com/embed/' + videoId;
	};

	$scope.playVideo = function(videoId){
		console.log("PLAY: " + videoId);
		YoutubeVideoPlayer.openVideo(videoId);
	}

    // Pedimos videos
    $scope.getLastestVideos();
}]);