angular.module('hdm').service('DownloadFilesSrv', ['$q', function ($q){

	var that = this;

	// Te devuelve una promesa con el objecto article
	// Si ya tenemos el fichero descargado se pone directamente, si no, lo descargamos 
	this.getLocalUrlOfFile = function(article){
		var fileUrl = article.image_url;
		var defered = $q.defer();

		this.fileIsDownloaded(fileUrl).then(function(fileIsDownloaded){
			if(fileIsDownloaded){
				console.log("fichero descargado");

				// Está descargado, asi que cojo la url, la pongo en el article y resuelvo la promesa
				var localFileUri = that.getUrlFromLocalStorage(fileUrl);
				article.image_local_url = localFileUri;
				defered.resolve(article);
			}else{
				console.log("fichero no descargado");

				// No está descargado
				that.downloadImage(article, defered);
			}
		}, function (error){});

		var promise = defered.promise;
		return promise;
	};

	this.downloadImage = function(article, defered){

		if(typeof(FileTransfer) != 'undefined'){
			var fileTransfer = new FileTransfer();
			var url = article.image_url;
			var filenamesplit = url.split("/");
			var filename = filenamesplit[filenamesplit.length-1];
			var localFilePath = ((ionic.Platform.isAndroid()) ? cordova.file.dataDirectory : cordova.file.documentsDirectory);
			//var localFilePath = cordova.file.dataDirectory;
			//var localFilePath = ((ionic.Platform.isAndroid()) ? cordova.file.dataDirectory : "/var/mobile/Applications/com.latteandcode.hdmapp/";
			var fileUri = localFilePath + filename;

			fileTransfer.download(
				url,
				fileUri,
				function(entry) {
					var localFileUri = entry.toURL();
					article.image_local_url = localFileUri;
					// Guardamos en local storage la url y la local_url
					that.saveLocalUrl(url, localFileUri);
					defered.resolve(article);
				},
				function(error) {
					console.log("error downloading");
					console.log(error);
					defered.reject(error);
				}
			);
		}else{
			//No estamos en móvil
			article.image_local_url = article.image_url;
			defered.resolve(article);
		}

		return true;
	};

	// Te dice si un fichero esta descargado en base a su url
	this.fileIsDownloaded = function(fileUrl){
		var defered = $q.defer();
		var urls = this.getUrlsFromLocalStorage();
		var fileIsDownloaded = false;

		if(typeof urls[fileUrl] == 'undefined'){
			// No está registrado en el localstorage
			defered.resolve(false);
		}else{
			// Está en el localstorage, vamos a ver si no ha sido borrado...
			var completeFilePath = urls[fileUrl];
			window.resolveLocalFileSystemURL(
				completeFilePath,
				function(){
					defered.resolve(true);
				},
				function(){
					defered.resolve(false);
				}
			);
		}

		var promise = defered.promise;
		return promise;
	};

	// Guarda una url y su local_url en el local storage
	this.saveLocalUrl = function(fileUrl, fileLocalUrl){
		var urls = this.getUrlsFromLocalStorage();
		urls[fileUrl] = fileLocalUrl;
		this.saveLocalUrlsInLocalStorage(urls);

		return true;
	};

	// Te devuele la local_url de una url
	this.getUrlFromLocalStorage = function(fileUrl){
		var urls = this.getUrlsFromLocalStorage();
		return urls[fileUrl];
	};

	// Te devuelve un array con las url descargadas y sus local_url
	this.getUrlsFromLocalStorage = function(){
		var urlsString = localStorage.getItem('local-files');
		urls = JSON.parse(urlsString);
		
		if(urls == null){
			urls = {};
		}

		return urls;
	};

	// Guarda el array de urls y local_url en el local storage 
	this.saveLocalUrlsInLocalStorage = function(urls){
		var urlsString = JSON.stringify(urls);
		localStorage.setItem('local-files', urlsString);

		return true;
	};
}]);