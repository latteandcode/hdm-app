angular.module('hdm').service('FeedSrv', ['$http', '$q', 'ArticleFactory', function ($http, $q, ArticleFactory) {

	var that = this;
	var urlBase = "https://hablandodemanzanas.com/get-articulos?page=";

	this.getLastestNews = function (page){
		var defered = $q.defer();
        var promise = defered.promise;

        var url = urlBase + page;

		$http.get(url).success(function(data) {
			//Building the articles
			var items = ArticleFactory.buildArticlesFromResponse(data);
			defered.resolve(items);
		}).error(function(err) {
			defered.reject(err);
		});

		return promise;
	};

}]);