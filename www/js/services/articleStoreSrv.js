angular.module('hdm').service('ArticleStoreSrv', ['$q', function ($q) {

	var that = this;

	// entity_node?fields=title,nid&parameters[type]=blog&parameters[status]=1&page=0&pagesize=30&sort=title&direction=DESC
	// var urlBase = 'https://hablandodemanzanas.com/node?page=0&limit=10&sort=created&direction=DESC';
	var urlBase = "https://hablandodemanzanas.com/get-articulos?page=0";

	this.getArticles = function(page){
		var articles = this.getArticlesFromLocalStorage();

		var sortable = [];

		for (var i in articles){
			article = articles[i];
			sortable.push(article);
		}

		sortable = sortable.sort(this.sortFunction);

		//Paginamos
		var primerElemento = page*10; 
		var ultimoElemento = (page+1)*10;
		var items = sortable.slice(primerElemento, ultimoElemento);

		return items;
	};

	this.sortFunction = function(a,b){
		if (a.created_sortable > b.created_sortable)
			return -1;
		if (b.created_sortable > a.created_sortable)
			return 1;
		return 0;
	};

	this.saveArticle = function (article){
		var articles = this.getArticlesFromLocalStorage();
		var nid = article.nid.toString();
		articles[nid] = article;
		this.saveArticlesToLocalStorage(articles);
	};

	this.saveArticles = function(articles){
		for(i in articles){
			article = articles[i];
			this.saveArticle(article);
		}

		return true;
	};

	this.getArticlesFromLocalStorage = function(){
		var articles = {};

		try{
			var articlesString = localStorage.getItem('articles');
			articles = JSON.parse(articlesString);
			if(articles == null){
				articles = {};
			}
		}catch(e){
			console.log("error !!");
			console.log(e);
		}

		return articles;
	};

	this.saveArticlesToLocalStorage = function(articles){
		var articlesString = JSON.stringify(articles);
		localStorage.setItem('articles', articlesString);

		return true;
	};

}]);