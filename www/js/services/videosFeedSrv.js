angular.module('hdm').service('VideosFeedSrv', ['$http', '$q', function ($http, $q) {

	var that = this;
	var urlBase = "https://www.googleapis.com/youtube/v3/search";
	var apiKey = "AIzaSyA1wILMoBhmHZmfnCiGIDUBfCSnDvMjRlk";

	this.getLastestVideos = function(){
		var defered = $q.defer();
        var promise = defered.promise;
		
		var youtubeParams = {
			key: apiKey,
			type: 'video',
			maxResults: '10',
			part: 'id,snippet',
			order: 'date',
			channelId: 'UCM3c6h8mxYUqKmKZdjSFpGA',
		};

		$http.get(urlBase, { params: youtubeParams }).success(function(data) {
			// Building the articles
			// var items = ArticleFactory.buildArticlesFromResponse(data);
			// defered.resolve(items);
			defered.resolve(data.items);
		}).error(function(err) {
			defered.reject(err);
		});

		return promise;
	};

}]);